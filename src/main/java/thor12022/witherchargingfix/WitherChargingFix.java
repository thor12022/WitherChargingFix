package thor12022.witherchargingfix;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.projectile.EntityWitherSkull;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.lang.reflect.Field;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Predicate;

@Mod(modid = WitherChargingFix.ID, name = WitherChargingFix.NAME, version = WitherChargingFix.VERSION, dependencies = WitherChargingFix.DEPEND)
public class WitherChargingFix
{

   public static final String NAME = "Wither Charging Fix";
   public static final String ID = "witherchargingfix";
   public static final String DEPEND = "";
   public static final String VERSION = "1.0.0";
   public static Logger logger = LogManager.getLogger(WitherChargingFix.NAME);

   @Mod.Instance
   public static WitherChargingFix instance;
   
   private boolean isEnabled = true;
   
   private static Predicate<Entity> attackEntitySelector = null;
 
   public WitherChargingFix()
   {
     MinecraftForge.EVENT_BUS.register(this);
   }
   
   @SubscribeEvent(priority=EventPriority.LOW)
   public void onLivingUpdate(LivingUpdateEvent event)
   {
      if(isEnabled && 
         !event.entity.worldObj.isRemote &&
         event.entityLiving != null &&
         event.entityLiving.getClass() == EntityWither.class)
      {
         EntityWither wither = (EntityWither)event.entityLiving;
         if(wither.getInvulTime() == 1)
         {
            if(attackEntitySelector == null)
            {
               // We can't retrieve the field from EntityWither at construction because Java doesn't initialize
               //    static data until an instance of the class has been created. A design decision I won't
               //    say anything about, I'll just leave you with these ellipses. . .
               try
               {
                  Field attackEntitySelectorField = EntityWither.class.getDeclaredField("field_82219_bJ");
                  attackEntitySelectorField.setAccessible(true);
                  attackEntitySelector = (Predicate<Entity>) attackEntitySelectorField.get(null);
               }
               catch(Exception e)
               {
                  try
                  {
                     Field attackEntitySelectorField = EntityWither.class.getDeclaredField("attackEntitySelector");
                     attackEntitySelectorField.setAccessible(true);
                     attackEntitySelector = (Predicate<Entity>) attackEntitySelectorField.get(null);
                  }
                  catch(Exception excp)
                  {
                   isEnabled = false;
                     WitherChargingFix.logger.warn("Cannot get Wither Attack Selector Field, disabling Vanilla Wither Bug Fix");
                  }
               }
            }
            wither.targetTasks.addTask(1, new EntityAIHurtByTarget(wither, false, new Class[0]));
            wither.targetTasks.addTask(2, new EntityAINearestAttackableTarget(wither, EntityLiving.class, 0, false, false, attackEntitySelector));
         }
      }
   }
   
   @SubscribeEvent(priority=EventPriority.HIGHEST)
   public void onEntityJoinWorld(EntityJoinWorldEvent event)
   {
      if(isEnabled &&
         !event.entity.worldObj.isRemote &&
         event.entity != null)
      {
         // Strip the Wither of it's targeting AI at spawn 
         if(event.entity.getClass() == EntityWither.class)
         {
            EntityWither wither = (EntityWither) event.entity;
            wither.targetTasks.taskEntries.clear();
         }
         // If we've loaded up a world with a Wither already firing away
         else if(event.entity.getClass() == EntityWitherSkull.class &&
               ((EntityWitherSkull)event.entity).shootingEntity != null &&
               ((EntityWitherSkull)event.entity).shootingEntity.getClass() == EntityWither.class && 
               ((EntityWither)((EntityWitherSkull)event.entity).shootingEntity).getInvulTime() > 0 )
         {
            event.setCanceled(true);
         }
      }         
   }
}
